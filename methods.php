<?php  

function validationCheck($value) {
    $value = trim($value);
    //$value = stripslashes($value);
    $value = htmlspecialchars($value);
    return $value;
}

function intToDateConverter($day, $mes, $year){
        $str_date = $year."-".$mes."-".$day;
        $d = strtotime($str_date);
        $date = date("Y-m-d", $d); 
        return $date;
}

?>