<?php

include "methods.php";

const URL = "https://www.google.com/recaptcha/api/siteverify";
const SECRET_KEY = "6LcnS5gUAAAAAEE0oGfY3s_RvSr5fHOXdBDJouR1";

//Define variables and set to empty
$name = $email = $phone = $date = $comment = "";

//if button 'send' is pressed
if(isset($_POST["send"])){

    //Construct query
    $query = URL.'?secret='.SECRET_KEY.'&response='.$_POST["g-recaptcha-response"];
    //Send query and decode data from json
    $data = json_decode(file_get_contents($query));

    //Check captcha validation
    if ($data->success == false)
        exit("Вы не прошли проверку капчи");

    #region Form Validation

    //Name Validation
    if(empty($_POST["name"])) {
        exit("Введите ФИО, пожалуйста :3");
    }
    else {
        $name = validationCheck($_POST["name"]);
        if (!preg_match("/^[a-zA-Zа-яА-Я ]+$/ui",$name)) {
            exit("Поле 'Имя' может содержать только буквы и пробелы.");
        }
    }

    //Email Validation
    if (empty($_POST["email"])) {
        exit("Заполните поле email");
      } 
      else {
        $email = validationCheck($_POST["email"]);
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          exit("Неверный формат email"); 
        }
      }

    ///Phone Validation
    $phone = validationCheck($_POST["phonenumber"]);
    if (!preg_match("/^[1-9]{3}[0-9]{4}[0-9]{4}$/",$phone)) {
        exit("Неферный формат номера");
    }

    $day   = $_POST["day"];
    $month = $_POST["month"];
    $year  = $_POST["year"];

    $date = intToDateConverter($day, $month, $year);

    //Comment validation
    $comment = validationCheck($_POST["message"]);

    #endregion

    #region SQL

    $connection = mysqli_connect('localhost','root','','aero');

    if(mysqli_connect_errno())
    {
        echo 'Ошибка в подключении к бд ('.mysqli_connect_errno().'): '.mysqli_connect_error();
        exit();
    }

    $sql = "INSERT INTO academy (FIO,phoneNumber, email, birthDate, comment) VALUES ('$name', '$phone', '$email', '$date', '$comment')";

    if ($connection->query($sql) == true) {
        echo "Новая запись добавлена успешно :)";
    }
    else {
        echo "Ошибка: " . $sql . "<br>" . $conn->error; 
    }

    $connection->close();

    #endregion

}
    
        
?>