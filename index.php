<!DOCTYPE html>
<html lang="en">
<head>
   
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

</head>
<body>


<div class="container h-100">
  <div class="row h-100 justify-content-center align-items-center">
    <form class="col-5" action="academy.php" method="post">
      <div class="form-group">
        <label for="formGroupExampleInput">Name:</label>
        <input name="name" type="text" class="form-control" id="formGroupExampleInput" placeholder="Enter your name">
      </div>
      <div class="form-group">
        <label for="formGroupExampleInput2">Phone:</label>
        <input name="phonenumber" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Enter your phone">
        <small id="emailHelp" class="form-text text-muted">пример: 89900000000</small>
      </div>
      <div class="form-group">
        <label for="formGroupExampleInput3">Email:</label>
        <input name="email" type="text" class="form-control" id="formGroupExampleInput3" placeholder="Enter your email">
      </div>
      <div class="form-group">
        <label for="formGroupBirthday">Дата рождения:</label>
        <div class="form-row">
          <div class="col">
            <input type="text" name="day" id="formGroupBirthday" class="form-control" placeholder="dd">
          </div>
          <div class="col">
            <input type="text" name="month" id="formGroupBirthday" class="form-control" placeholder="mm">
          </div>
          <div class="col">
            <input type="text" name="year" id="formGroupBirthday" class="form-control" placeholder="yyyy">
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="formGroupExampleInput4">Message:</label>
        <textarea name="message" class="form-control" id="formGroupExampleInput4" placeholder="Leave a comment here"></textarea>
      </div>

      <div class="g-recaptcha" data-sitekey="6LcnS5gUAAAAADxBrgnZ0-wgH2vB7I_PcU-AznbR"></div> <br/>
      <input type="submit" class="btn btn-primary mb-2" value="Отправить" name="send">
    </form>   
  </div>
</div>

</body>
</html>